- Hessel Dijkstra
- Joukje Kloosterman
- Keimpe Dijkstra
- Wesley Rorije
- Wouter Zeevat

21/01/2022
1.0.0

# Fastq files analyser

## Description
By running this pipeline, your fastq files will be analysed. This will the unzip the files. check their quality, trimming them,
aligning them as well as preprocessing and finally creating a pdf file with the analysis. The output file will contain information
such as the quality, what kind of files they are (colorspace, paired end) and more.
The pipeline handles the downloading of the reference files (gtf, ht2). Before running the pipeline, make sure you have followed
every step in the installation section below!

## Installation
### 1:
In order for the pipeline to work, you will need to run it on a lunix system,
Could be an ssh connection.
### 2: installing tools
Make sure to download the following tools before running the pipeline.

- hisat2 : http://daehwankimlab.github.io/hisat2/
- fpdf
- picard: https://github.com/broadinstitute/picard 
The pipeline uses 3 more tools, these are already downloaded.
### 3: Changing the tool paths
After installing the hisat2 tool and the picard tool, open the Changed_Code/aligningMain.py file 
and change the path of the hisat variable to your hisat2 tool's location.

## Usage
* python3 path/to/aligningMain.py -d fastqdir -o organism -out outputdir [-len length] [-p threads] [-m masking] [-l level] [-cn chromosome number] 
arguments:

#### fastqdir
The path to the directory containing your fastq files.
- required
#### organism: 
The organism your files will be referenced on.
- required
#### outputdir
The path to the directory that will have the output files.
- required
#### length
Discard reads that became shorter than length INT because 
of either quality or adapter trimming. A value of '0' effectively disables this behaviour.
- default 20
#### threads
The number of threads the pipeline will use
- default None
#### masking
The type of masking to be used in the genome file
- default dna_sm
#### level
The assembly type for the genome file, default is primary assembly
- default primary_assembly
#### chromosome number
If the level argument is chromosome, this argument counts as the
number of the chromosome
- default ''

example: 
  python3 aligningMain.py -d /students/2019-2020/Thema06/project-data/How_to_deal_with_difficult_data/RawFiles/ -o "homo_sapiens" -out /students/2021-2022/Thema06/jkloosterman2

# Support
If you struggle to install the pipeline, found a bug or need other support. Please send an
email to one of these addresses.

- h.u.dijkstra@st.hanze.nl
- ke.dijkstra@st.hanze.nl
- j.kloosterman@st.hanze.nl
- w.a.rorije@st.hanze.nl
- w.h.zeevat@st.hanze.nl