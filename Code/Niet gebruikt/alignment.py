#!usr/bin/env python3

"""
Alignment function made seperatly, just made to paste into pipeline
"""

import glob
import subprocess
import re

hisat = 'hisat2' # already on the system
outputDir = 'output' # The output directory, already defined in the alignmentMain.py
threads = 2  # Already defined
genomeHiSat2 = '/students/2021-2022/Thema06/jkloosterman/Genomes/Hisat2/homo_sapiens/GRCh38' # Path to ht2 files minus
# Return the path above as genomehisat2 in determineGenomeInfo
def alignment(ext, genome_hisat_2):
    """
		The actual alignment is performed in this function.
		The trimmed reads are obtained from the trimmed folder and are used of the alignment.
		The log file of the alignment is written to the
		Results/alignment folder and the .bam file is created.

    ------------
    Attributes
    ------------
    ext : str
        extention of trimmed fastq files. example: 'fastq'
    genome_hisat_2 : str
        path to the file to the f2a index file, minus the .*.f2a
    """

    paired_files = []
    unique_file_names = []
    for files in glob.glob(outputDir + "/Preprocessing/trimmed/*." + ext):
        if files not in unique_file_names:
            unique_file_names.append(files)
            file_name = files.replace('_trimmed', '')
            if re.match(r'.*_[1-2].' + ext, file_name):  # If paired-end file
                if file_name[:file_name.find('_')] not in paired_files:
                    paired_files.append(file_name[:file_name.find('_')])
                continue

            name = files.split("/")
            fastq_name, ext = name[-1].split(".")
            subprocess.call(f'{hisat} -x {genome_hisat_2} -U {files} -p {str(threads)}'
                    f' 2>> {outputDir}/Results/alignment/{fastq_name.replace("_trimmed","")}.log |'
                    f' samtools view -Sbo {outputDir}/Preprocessing/aligned/'
                    f'{fastq_name.replace("_trimmed", "")}.bam -', shell=True)
    for paired_file in paired_files:
        file_name = re.search(r'\/[^\/]*$', paired_file)[0][1:].split('.')[0]
        subprocess.call(f'{hisat} -f -x {genome_hisat_2} -1 {paired_file + "_1." + ext}'
                        f' -2 {paired_file + "_2." + ext} -p {str(threads)} 2>> '
                        f'{outputDir}/Results/alignment/{file_name}.log |'
                        f' samtools view -Sbo {outputDir}/Preprocessing/aligned'
                        f'{file_name}.bam -', shell=True)

alignment('fq', genomeHiSat2)
