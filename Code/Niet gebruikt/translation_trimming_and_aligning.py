#!usr/bin/env python3

"""
The 3 tools that need to be added in the main script.
"""

import glob
import os
import subprocess
import re

trim = None
#------------------
perl_translation_script = '/homes/whzeevat/thema6/thema06/Changed_Code/translate_to_letter_space.pl'
#------------------

# The output directory, already defined in the alignmentMain.py
outputDir = '/homes/whzeevat/thema6/thema06/Code/output'
trimGalore = "trim_galore"
fastqDir = '/homes/whzeevat/thema6/thema06/Test_Data/fastq_gz/'
hisat = 'hisat2' # already on the system
threads = 2  # Already defined
# Path to ht2 files minus
genomeHiSat2 = '/students/2021-2022/Thema06/jkloosterman2/Genomes/Hisat2/homo_sapiens/GRCh38'

def is_paired_file(file):
    '''
        A function that determines whether a file is a paired end file
        It returns either a 1, 2 or False
    ------------
    Attributes
    ------------
    file : str
    a path to a file, example 'home/example_1.fastq'
    '''
    file_name = re.search(r'\/[^\/]*$', file)[0][1:].split('.')[0]
    if '_1' in file_name:
        return 1
    if '_2' in file_name:
        return 2
    return False


def translate_color_spaces():
    '''
        Translate the files that contain color spaces to letter spaces.
        also moves all files that it unzips to the rawdata fastq folder.
    '''
    for files in glob.glob(fastqDir + "*.gz"):
        # The filename, extention and the 'gz' are obtained
        filesNameComp = files.split("/")[-1]
        filesName, ext, gz = filesNameComp.split(".")
        # Copy-paste the gzipped file to the RawData directory
        if not os.path.exists(outputDir + "/RawData/fastqFiles/" + filesNameComp):
            os.system("cp " + files + " " + outputDir + "/RawData/fastqFiles/" + filesNameComp)

        if not os.path.exists(outputDir + "/RawData/fastqFiles/" + filesName + "." + ext):
            os.system("gunzip -k " + outputDir + "/RawData/fastqFiles/" + filesNameComp)

        input = outputDir + '/RawData/fastqFiles/'\
                + filesNameComp.replace("." + ext + ".gz", "." + ext)
        with open(input) as file:
            line = file.readlines()[1]
            try:
                # Checking if the last character is a number
                # to see if the file is written in color space
                int(line.strip()[-1])

                is_paired = is_paired_file(input)
                if is_paired:  # Replacing
                    output = input.replace('_' + str(is_paired) + '.' + ext,
                                           '-wasColorSpace_' + str(is_paired) + '.' + ext)
                else:
                    output = input.replace('.' + ext, '-wasColorSpace.' + ext)
                os.system(f'perl {perl_translation_script} -fastq {input} -o {output}')
                os.remove(input) # Removing untranslated files
            except ValueError:
                continue

def trimFiles(ext):
    '''
        The files are trimmed based on trim_galore. Whenever the
        files are paired end, the function will use the tool in a different way
        to trim them correctly
    ------------
    Attributes
    ------------
    ext : str
    extention of trimmed fastq files. example: 'fastq'
    '''
    paired_files = []
    for file in glob.glob(outputDir + "/RawData/fastqFiles/*." + ext):

        # Checks whether the files are paired end
        is_paired = is_paired_file(file)
        if is_paired:
            if file.replace('_' + str(is_paired), '') not in paired_files:
                paired_files.append(file.replace('_' + str(is_paired), ''))
        else:
            os.system(f'{trimGalore} --path_to_cutadapt /usr/bin/cutadapt {file}'
                      f' -o {outputDir}/Preprocessing/trimmed/')

    for file in paired_files:

        # The paired end files are trimmed a different way
        path = file.replace('.' + ext, '')
        # Removes the path to get the filename (home/test.fastq -> test)
        base_name = re.search(r'\/[^\/]*$', file)[0][1:].split('.')[0]
        os.system(f'{trimGalore} --path_to_cutadapt /usr/bin/cutadapt --paired '
                  f'{path}_1.{ext} {path}_2.{ext}'
                  f' --basename {base_name} -o {outputDir}/Preprocessing/trimmed/')

def alignment(ext, genome_hisat_2):
    """
		The actual alignment is performed in this function.
		The trimmed reads are obtained from the trimmed folder and are used of the alignment.
		The log file of the alignment is written to the
		Results/alignment folder and the .bam file is created.

    ------------
    Attributes
    ------------
    ext : str
        extention of trimmed fastq files. example: 'fastq'
    genome_hisat_2 : str
        path to the file to the f2a index file, minus the .*.f2a
    """

    paired_files = []
    unique_file_names = []
    for file in glob.glob(outputDir + "/Preprocessing/trimmed/*" + ext):
        if file not in unique_file_names:
            unique_file_names.append(file)
            file_name = file.replace('_trimmed', '')

            # Paired end file paths are getting stored and will be executed later
            is_paired = is_paired_file(file)
            if is_paired:
                path = file.replace('_' + str(is_paired) + '.' + ext, '')
                if path not in paired_files:
                    paired_files.append(path)
                continue

            name = file.split("/")
            fastq_name, ext = name[-1].split(".")
            subprocess.call(f'{hisat} -x {genome_hisat_2} -U {file} -p {str(threads)}'
                    f' 2>> {outputDir}/Results/alignment/{fastq_name.replace("_trimmed","")}.log |'
                    f' samtools view -Sbo {outputDir}/Preprocessing/aligned/'
                    f'{fastq_name.replace("_trimmed", "")}.bam -', shell=True)

    for paired_file in paired_files:
        # The paired end data gets aligned with different settings
        file_name = re.search(r'\/[^\/]*$', paired_file)[0][1:].split('.')[0]
        subprocess.call(f'{hisat} -x {genome_hisat_2} -1 {paired_file + "_1." + ext}'
                        f' -2 {paired_file + "_2." + ext} -p {str(threads)} 2>> '
                        f'{outputDir}/Results/alignment/{file_name}.log |'
                        f' samtools view -Sbo {outputDir}/Preprocessing/aligned'
                        f'{file_name}.bam -', shell=True)

translate_color_spaces()
trimFiles('fastq')
alignment('fq', genomeHiSat2)
