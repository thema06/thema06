#!/usr/bin/python3
"""
Authors: 	H. Dijkstra, K. Dijkstra, J. Kloosterman, W. Rorije & W. Zeevat
Date:		21-01-2021
Purpose:	Creates a pdf with the data that is created in the data_reader.py module.

"""
import re
from fpdf import FPDF, HTMLMixin
import data_reader


class MyFPDF(FPDF, HTMLMixin):
    """ A class that is necessary to write to a pdf."""
    pass


class CreatePDF:
    """ This class creates a pdf."""
    def __init__(self, outputdir, unique_fq_files):
        """
        Initialize variables

        ...

        Attributes
        ------------
        outputdir : str
            directory where all the necessary files are to create the pdf
        unique_fq_files : list
            all unique fastq filenames
        """
        self.outputdir = outputdir
        self.pdf = MyFPDF()
        self.fq_files = unique_fq_files
        self.sam = data_reader.SamFileCheck()
        self.mtDNA_stats = self.sam.mt_check(self.outputdir)
        self.header, self.data = data_reader.get_count_results(self.outputdir)
        self.alignment_results, self.alignment_problems = data_reader.get_alignment_result(self.outputdir)
        self.trim_results = data_reader.get_trim_results("../Test_Data/")

    def read_files(self, color_space):
        """
        Read files during pipeline proces and filetype.
        ...

        Attributes
        ------------
        color_space : list with colorspace filenames
        """
        self.pdf.write_html(text='<h3>Read files</h3>')

        read_files = f"""<table border="1" align="right" width="100%">
                <thead><tr><th width="30%"> Filename </th><th width="35%"> Type </th>
                <th width="35%"> Paired or single end </th></tr></thead>
                <tbody>"""

        for fq_file in self.fq_files:
            if fq_file in color_space:
                file_type = "SOLiD ColorSpace FASTQ"
            else:
                file_type = "Standard(Sanger) FASTQ"
            if re.match(r'.*_[1-2]', fq_file):
                s_p_end = "Paired end"
            else:
                s_p_end = "Single end"
            read_files += f""" <tr><td width="30%" align="center">{fq_file}</td>
                            <td width="35%" align="center">{file_type}</td>
                            <td width="35%" align="center">{s_p_end}</td></tr> """

        read_files += f"""</tbody></table>"""

        self.pdf.write_html(text=read_files)

        self.pdf.ln()

    def count_matrix(self):
        """writing of count file matrix file to pdf in html tables"""
        self.pdf.write_html(text='<h3>Count file matrix</h3>')

        number_of_files = len(self.data["Assigned"])

        current_file = 0
        for x in range(number_of_files):
            structure = f""" <table border="1" align="center" width="100%">
                               <thead><tr><th width="22%"> info </th></tr>
                               <tr><th width="78%">{self.header[current_file-1]} </th></tr>
                               </thead><tbody>"""

            for matrix in self.data:

                structure += f""" 
                           <tr>
                               <td width = "22%" align="center">{matrix}</td>
                               <td width = "78%" align="center">{self.data[matrix][current_file]}</td>
                           </tr>
                        """

            structure += f"""</tbody> </table> """
            current_file += 1
            self.pdf.write_html(text=structure)

    def settings_pdf(self):
        """ Takes settings for pdf like fontsize pdf format line heights"""
        # Add a page
        self.pdf.add_page(format='a3')

        # text type for writing
        # pdf.set_font("Arial", size=15)

    def write_introduction(self):
        """ Introduction part of pdf file"""
        self.pdf.write_html(text="<center><h1>Report</h1></center>")
        self.pdf.write_html(text="<p>This report describes the problems that were found in the different files."
                                 " First there is an overview of all the files, which type these files are and if"
                                 " it is a paired end or single end file. Then, it reports if there were "
                                 "problems found in one of the files and it describes extra steps that were"
                                 " taken to compensate the data. There is also a short overview of how much "
                                 "mitochondrial DNA was found per sam file. Be aware that paired end files are generate"
                                 " a single sam file. After that there is a summary of how the files were trimmed."
                                 " Also, there is a overview of how the files are aligned and possible problems are "
                                 "addressed. The alignment results in a count matrix. Finally there is a discription "
                                 "where the MultiQC html page can be found where all of the quality scores, "
                                 "markduplicates, alignments and trimming data can be found.</p>")

        # linebreak takes heigth of last printed cell
        self.pdf.ln()

    def found_problems(self, color_space, corrupt_files):
        """
        Writes the found problems on a biological spectrum to the pdf.
        ...

        Attributes
        ------------
        color_space : list with colorspace filenames
        corrupt_files : list with corrupted filenames
        """
        self.pdf.write_html(text='<h3>Found problems</h3>')
        having_color_space = False
        if len(color_space) > 0:
            having_color_space = True
            self.pdf.write_html(text='<p>FASTQ files in SOLiD ColorSpace format:</p>')
            for color_file in color_space:
                self.pdf.write_html(text=f'<p>- {color_file}</p>')

        self.pdf.ln()

        if len(corrupt_files) > 0:
            self.pdf.write_html(text='<p>FASTQ files contained very low quality scores:</p>')
            for corrupt_file in corrupt_files:
                self.pdf.write_html(text=f'<p>- {corrupt_file}</p>')

        self.pdf.ln()

        return having_color_space

    def extra_steps_taken(self, having_color_space):
        """
        Defines extra steps taken to enrich or alter data to pdf report
        ...

        Attributes
        ------------
        having_color_space: boolean for having (or not) colorspace files
        """
        self.pdf.write_html(text='<h3>Extra steps taken</h3>')
        if having_color_space:
            self.pdf.write_html(text='<p>The FASTQ files in ColorSpace format were translated to '
                                     'Standard(Sanger) FASTQ. </p>')
        self.pdf.ln()

    def summary(self):
        """ Writes the summary of the reading proces read_lines, stripped lines,unassigned values to pdf report"""
        self.pdf.write_html(text='<h3>Summary of trimmed files</h3>')

        for key, value in self.trim_results.items():
            summary_structure = f""" <table border="1" align="center" width="100%">
                                           <thead><tr><th width="60%"> info </th></tr>
                                           <tr><th width="40%"> {key} </th></tr>
                                           </thead><tbody>"""

            counter = 0
            for info in value:
                splitted_info = info.split(":")
                elements_count = len(splitted_info)

                if elements_count == 1:
                    summary_structure += f""" 
                    <tr>
                        <td width = "60%" align="center">Processed</td>
                        <td width = "40%" align="center">{splitted_info[0]}</td>
                    </tr>
                    """
                else:
                    summary_structure += f""" 
                    <tr>
                        <td width = "60%" align="center">{splitted_info[0]}</td>
                        <td width = "40%" align="center">{splitted_info[1].strip()}</td>
                    </tr>
                    """
                counter += 1

            summary_structure += f"""</tbody> </table> """

            self.pdf.write_html(summary_structure)

    def alignment(self):
        self.pdf.write_html(text='<h3>Alignment Results</h3>')
        for file_name in self.alignment_results:
            self.pdf.write_html(text=f"""<p>{file_name}</p>""")
            for lines in self.alignment_results[file_name]:
                self.pdf.write_html(text=f"""<p>{lines}</p>""")
            if file_name in self.alignment_problems:
                self.pdf.write_html(text=f"""<p>{self.alignment_problems[file_name]}</p>""")
            self.pdf.ln()

    def mitochondrial_dna(self):
        self.pdf.write_html(text='<h3>Mitochondrial DNA</h3>')
        for sam_file in self.mtDNA_stats:
            self.pdf.write_html(text=f"""<p>{sam_file}</p>
            <p>    Non-mitochondrial DNA: {self.mtDNA_stats[sam_file][0]}</p>
            <p>    Mitochondrial DNA: {self.mtDNA_stats[sam_file][1]}</p>
            <p>    Percentage mitochondrial DNA: {self.mtDNA_stats[sam_file][2]}%</p>""")
            self.pdf.ln()

    def quality_scores(self):
        self.pdf.write_html(text='<h3>MultiQC</h3>')
        self.pdf.write_html(text=f"""
        <p>You can find the MultiQC html file in the /Results/multiqc directory.</p>
        <p>Open this file in your browser to see all the quality scores, markduplicate, alignment and trimming data.</p>
        """)
        self.pdf.ln()

    def create_pdf(self, color_space, corrupt_file):
        """
        Function for creating pdf file, it calls other functions from the class CreatePDF.
        ...

        Attributes
        ------------
        color_space : list with colorspace filenames
        corrupt_file : list with corrupted filenames
        """

        self.settings_pdf()
        self.write_introduction()
        self.read_files(color_space)
        having_color_space = self.found_problems(color_space, corrupt_file)
        self.extra_steps_taken(having_color_space)
        self.mitochondrial_dna()
        self.summary()
        self.alignment()
        self.count_matrix()
        self.quality_scores()

        # save the pdf with name .pdf

        self.pdf.output(self.outputdir+"/Results/pdf/"+"report.pdf")


def main():
    c_pdf = CreatePDF("/students/2021-2022/Thema06/jkloosterman12", ["SRR1106118", "SRR1106122", "SRR1106138",
                                                                     "SRR1106139", "SRR1106140", 'SRR018015',
                                                                     "SRR018013_1", "SRR018013_2", "SRR057598",
                                                                     "SRR057599"])
    c_pdf.create_pdf(['SRR018015', "SRR018013_1", "SRR018013_2"], ["SRR1106118", "SRR1106122", "SRR1106138",
                                                                   "SRR1106139", "SRR1106140"])


if __name__ == "__main__":
    main()
