# imports
import glob
import os
import shutil
import subprocess
import urllib.request as request
from contextlib import closing

import json
import requests
import sys


class DownloadGenomeData:
    """
    Class which connects to the ensembl server and retrieves genome files

    ...

    Attributes
    ------------
    organism : str
        String containing the organism type, visit the ensembl site
        to see all available organisms.
        Should be in lowercase
        example: homo_sapiens

    ...

    Usage example
    ------------
    organism = "homo_sapiens"

    download_object = download_genome_data(organism)

    assembly_accession = download_object.assembly_name()

    output = download_object.genome_file_name(assembly_accession, masking="dna",
    level="chromosome", chromosome_number=str(2))
    """

    def __init__(self, organism, genome_folder):
        organism = organism.lower()

        self.server = "https://rest.ensembl.org"
        self.genome_folder = genome_folder + "/Genomes/"
        self.gtf_folder = self.genome_folder + "gtf/"
        self.hisat_folder = self.genome_folder + "HiSat2/" + organism
        self.organism = organism
        self.url = "http://ftp.ensembl.org/pub/release-105/fasta/" + self.organism + "/dna/"
        self.url_gtf = "http://ftp.ensembl.org/pub/release-105/gtf/" + self.organism + "/"

    def getrequest(self, ext):
        """
        Function makes a request object to connect to the ftp server

        ...

        Attributes
        ------------
        ext : str
            directory where the requested file is stored,
            doesn't include the server dir
        """
        r = requests.get(self.server + ext, headers={"Content-Type": "application/json"})

        if not r.ok:
            r.raise_for_status()
            sys.exit()

        return r

    def check_current_dir(self, directory, folder):
        """
        Check whether the requested organism genome is already in the genome folder

        ...

        Attributes
        ------------
        folder : str
            directory where the genome file is to be stored
        """

        for filename in os.listdir(folder):
            if folder + filename == directory[:-3]:
                return True

        return False

    def unzip_gz(self, genome_dir):
        """
        unzips .gz files

        ...

        Attributes
        ------------
        genome_dir : str
            directory where the genome file has to be stored
        """
        os.system('gzip -d ' + genome_dir)
        return genome_dir[:-3]

    def assembly_name(self):
        """
        Retrieves the assembly name for a given organism
        """
        ext = "/info/assembly/" + self.organism + "?"

        r = self.getrequest(ext)

        decoded = r.json()
        info = repr(decoded)

        info = info.replace("\'", "\"")
        info = info.replace("None", "\"None\"")
        info = json.loads(info)

        assembly_accession = info["assembly_name"].split(".")[0]

        return assembly_accession

    def genome_file_name(self, assembly_accession, masking="dna_sm", level="primary_assembly", chromosome_number=""):
        """
        This function makes the name of the genome file that has to be downloaded.

        ...

        Attributes
        ------------
        assembly_accession : str
            A string that contains the assembly name of the organism
        masking : str
            DEFAULT : dna_sm
            A string that specifies the masking type used (the way repeats are handled)
                *dna_sm -> convert repeats into N's
                *dna_rm -> convert repeats into lowercase
                *dna    -> doesn't mask repeats
        level : str
            DEFAULT : primary_assembly
            A string that determines the type of file we want to use.
            For example: primary assembly, chromosome, etc.
        chromosome_number : str
            DEFAULT : ""
            A integer converted to a string, that specifies the chromosome number when level is chromosome
        """

        file_download = self.organism.capitalize() + "." + assembly_accession + "." + masking + "." + level + "." \
            + chromosome_number + ".fa.gz"
        file_download = file_download.replace("...", ".")
        file_download = file_download.replace("..", ".")

        genome_dir = self.genome_folder + file_download
        url_file = self.url + file_download
        folder = self.genome_folder
        genome_dir = self.download_main(genome_dir, url_file, file_download, folder)

        return genome_dir

    def gtf_file_name(self, assembly_accession):
        """
        This function makes the name of the gtf file that has to be downloaded.

        ...

        Attributes
        ------------
        assembly_accession : str
            A string that contains the assembly name of the organism
        """
        file_download = self.organism.capitalize() + "." + assembly_accession + ".105.gtf.gz"
        file_download = file_download.replace("...", ".")
        file_download = file_download.replace("..", ".")

        gtf_dir = self.gtf_folder + file_download
        url_file = self.url_gtf + file_download
        folder = self.gtf_folder
        gtf_dir = self.download_main(gtf_dir, url_file, file_download, folder)
        return gtf_dir

    def download_main(self, directory, url, file_download, folder):
        """
        Function that oversees the download of the file,
        calls upon multiple functions

        ...

        Attributes
        ------------
        directory : str
            A string that contains the directory where the file is/needs to be saved
        url : str
            A string that contains the url from where the file can be downloaded
        file_download : str
            The name of the file that has to be downloaded
        folder : str
            The directory of the folder where the file is saved (local)
        """
        if self.check_current_dir(directory, folder):
            return directory[:-3]
        else:
            try:
                self.download_genome_file(directory, url)
                directory = self.unzip_gz(directory)
                return directory
            except:
                print("error, check if ", file_download, " is correct")

    def download_genome_file(self, genome_dir, url_file):
        """
        Function that downloads the file from the ensembl FTP server

        ...

        Attributes
        ------------
        genome_dir : str
            directory where the genome file has to be stored
        url_file : str
            file that has to be downloaded
        """

        with closing(request.urlopen(url_file)) as r:
            with open(genome_dir, 'wb') as f:
                shutil.copyfileobj(r, f)

    def genomeHiSat2(self, assembly_accession, genome_dir):
        """
        Function that returns the folder where the .ht2 files are stored
        for the given organism
        ...

        Attributes
        ------------
        genome_dir : str
            directory where the genome file has to be stored
        assembly_accession : str
            A string that contains the assembly name of the organism
        """
        if glob.glob(self.hisat_folder + "/" + assembly_accession + ".*.ht2"):
            return self.hisat_folder + "/" + assembly_accession
        else:
            subprocess.call("hisat2-build " + genome_dir + " " + self.hisat_folder + "/" + assembly_accession,
                            shell=True)
            return self.hisat_folder + "/" + assembly_accession


def main():
    organism = "homo_sapiens"
    folder = "/students/2021-2022/Thema06/jkloosterman"
    download_object = DownloadGenomeData(organism, folder)

    assembly_accession = download_object.assembly_name()

    output = download_object.genome_file_name(assembly_accession, masking="dna", level="chromosome",
                                              chromosome_number=str(2))
    print(output)

    output = download_object.gtf_file_name(assembly_accession)
    print(output)

    output = download_object.genomeHiSat2(assembly_accession)
    print(output)


if __name__ == "__main__":
    main()
