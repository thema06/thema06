#!/usr/bin/python3

"""
Authors: 	H. Dijkstra, K. Dijkstra, J. Kloosterman, W. Rorije & W. Zeevat
Date:		21-01-2021
Purpose:	Creating directories for the alignment pipeline
"""

# Imports
import os


def buildOutputDir(outputdir):
	'''
	Check if the output directory already exists, otherwise create it.
	'''
	if not os.path.exists(outputdir):
		os.makedirs(outputdir)

def extendOutputDir(outputdir):
	"""
	Check if the preprocessing folder already exists, if this is not the
	case. Create this directory with the other folders.
	"""
	if not os.path.exists(outputdir + "/Preprocessing/"):
		os.makedirs(outputdir + "/Preprocessing/")
		os.makedirs(outputdir + "/Preprocessing/trimmed")
		os.makedirs(outputdir + "/Preprocessing/aligned")
		os.makedirs(outputdir + "/Preprocessing/sortedBam")
		os.makedirs(outputdir + "/Preprocessing/addOrReplace")
		os.makedirs(outputdir + "/Preprocessing/mergeSam")
		os.makedirs(outputdir + "/Preprocessing/markDuplicates")
		os.makedirs(outputdir + "/Preprocessing/samFiles")

def createResultDir(outputdir):
	'''
	Check if the results directory exists, otherwise create it.
	'''
	if not os.path.exists(outputdir + "/Results/"):
		os.makedirs(outputdir + "/Results/")
		os.makedirs(outputdir + "/Results/alignment")
		os.makedirs(outputdir + "/Results/fastQC")
		os.makedirs(outputdir + "/Results/multiQC")
		os.makedirs(outputdir + "/Results/pdf")
########################################################################
def createCodeDir(outputDir):
	'''
	Check if the code directory exists, otherwise create it.
	'''
	if not os.path.exists(outputDir + "/Code/"):
		os.makedirs(outputDir + "/Code/")
		os.makedirs(outputDir + "/Code/aligningPipeline")
		
def createRawDataDir(outputDir):
	'''
	Check if the code directory exists, otherwise create it.
	'''
	if not os.path.exists(outputDir + "/RawData/"):
		os.makedirs(outputDir + "/RawData/")
		os.makedirs(outputDir + "/RawData/fastqFiles")
		os.makedirs(outputDir + "/RawData/counts")


def createGenomesDir(outputDir, organism):
	if not os.path.exists(outputDir + "/Genomes/"):
		os.makedirs(outputDir + "/Genomes/")
		os.makedirs(outputDir + "/Genomes/gtf")
		os.makedirs(outputDir + "/Genomes/HiSat2/" + organism)
	elif not os.path.exists(outputDir + "/Genomes/HiSat2/" + organism):
		os.makedirs(outputDir + "/Genomes/HiSat2/" + organism)


def createAllDirs(outputDir, organism):
	buildOutputDir(outputDir)
	extendOutputDir(outputDir)
	createResultDir(outputDir)
	createCodeDir(outputDir)
	createRawDataDir(outputDir)
	createGenomesDir(outputDir, organism)
	
########################################################################
if __name__ == "__main__":
	directory = "/home/mldubbelaar/Desktop/testDir/"
	createAllDirs(directory, "homo_sapiens")
	
