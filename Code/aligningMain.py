#!/usr/bin/python3

"""
Authors: 	H. Dijkstra, K. Dijkstra, J. Kloosterman, W. Rorije & W. Zeevat
Date:		21-01-2021
Purpose   : Act as a main for the alignment procedure.
"""

import os
import re
import glob
import shutil
import subprocess
import argparse

# Import the self-made files
import createDirs
from ftp_download import DownloadGenomeData

from pdf_generator import CreatePDF

# Tools used in this project
picard = "../Tools/picard/build/libs/picard.jar"
hisat = "/usr/bin/hisat2"
featureCounts = "../Tools/subread-2.0.3-Linux-x86_64/bin/featureCounts"
trimGalore = "../Tools/TrimGalore-0.6.6/trim_galore"
colorSpaceTranslation = "translate_to_letter_space.pl"

# Variables
fastqDir = ""
outputDir = ""
organism = ""
seqType = ""
threads = ""
length = ""
masking = ""
level = ""
chromosome_number = ""

unique_files = []
corrupt_files = []
paired_files = []
single_files = []
color_space_files = []

color_print = "\033[4;36;40m\033[1;36;40m"
white_print = "\033[0;37;40m"


# Annotated function
def arguments():
    """
    This functions defines the different parameters used for this function and
    gives a help output if the parameters are not given.
    """
    parser = argparse.ArgumentParser()

    parser.add_argument('-d', '--fastqDir', help='Directory to the fq.gz/fastq.gz files', required=True)
    parser.add_argument('-o', '--organism',
                        help='Define the name of the organism as it is in the ensembl database (ex:homo_sapiens, '
                             'mus_musculus)',
                        required=True)
    parser.add_argument('-out', '--outputDir', help='Pathway to output directory', required=True)
    parser.add_argument('-len', '--length', help="""Discard reads that became shorter than length INT because 
    of either quality or adapter trimming. A value of '0' effectively disables this behaviour. Default: 20 bp.""",
                        default=20)
    parser.add_argument('-s', '--seqType', help='Define SE for single end sequencing or PE for paired end sequencing')
    parser.add_argument('-p', '--threads', help='Define number of threads to use')
    parser.add_argument('-m', '--masking',
                        help='Define the type of masking to be used in the genome file, default is dna_sm',
                        default='dna_sm')
    parser.add_argument('-l', '--level',
                        help='Define the assembly type for the genome file, default is primary assembly',
                        default='primary_assembly')
    parser.add_argument('-cn', '--chromosome_number',
                        help='When the level argument is chromosome, define the chromosome number in this argument',
                        default='')
    # Save all the defined parameters in the variable args.
    args = parser.parse_args()

    # define the global variables, enabling local changes.
    global fastqDir
    global outputDir
    global organism
    global seqType
    global threads
    global length
    global masking
    global level
    global chromosome_number

    fastqDir = args.fastqDir
    outputDir = args.outputDir
    organism = args.organism
    seqType = args.seqType
    threads = args.threads
    length = str(args.length)
    masking = args.masking
    level = args.level
    chromosome_number = args.chromosome_number

    if threads is None:
        # The half of the number of threads in the PC is used when the number of threads is not defined
        process = subprocess.run("grep -c processor /proc/cpuinfo", shell=True, check=True, stdout=subprocess.PIPE,
                                 universal_newlines=True)
        output = process.stdout
        threads = str(int(int(output) / 2))
    else:
        threads = str(threads)


def fasta_processing(genome_fasta):
    """
    The first step is to determine if the additional dict and index file of the organism fasta are created.

    ...

    Attributes
    ------------
    genome_fasta: str
        directory where the genome fasta file is stored
    """
    print(color_print + "*** CHECKING AND CREATING NECESSARY ADDITIONAL FILES OF GENOME FASTA files ***"
          + white_print + "\n")
    # Determine if the fasta.dict has been created already
    if not os.path.isfile(genome_fasta.replace("fa", "dict")):
        # If this is not the case, the file will be created
        subprocess.run("java -jar " + picard + " CreateSequenceDictionary R=" + genome_fasta, shell=True)

    # Determine if the fasta.fa.fai has been created already
    if not os.path.isfile(genome_fasta + "fai"):
        # If this is not the case, the file will be created
        subprocess.run("samtools faidx " + genome_fasta, shell=True)


def quality_check():
    """ The function quality_check performs the fastQC check and writes the output to the Results/fastQC/ folder."""
    print(color_print + "*** PERFORM QUALITY CHECK ***" + white_print + "\n")
    subprocess.run("fastqc " + fastqDir + "*.gz -o " + outputDir + "/Results/fastQC/ -t " + threads, shell=True)


def is_paired_file(fq_file_name):
    """
        A function that determines whether a file is a paired end file
        It returns either a 1, 2 or False
    ------------
    Attributes
    ------------
    fq_file_name : str
        name of fastq files
    """

    if '_1' in fq_file_name:
        return "1"
    elif '_2' in fq_file_name:
        return "2"
    else:
        return False


def translate_color_spaces(directory, fq_file_name, ext):
    """
        Translate the files that contain color spaces to letter spaces.

        ...

        Attributes
        ------------
        directory : str
            directory where the output files are stored
        fq_file_name: str
            list of FQ file names
        ext: str
            extension fq or fastq

    """
    path_to_input = directory + fq_file_name + "." + ext
    with open(path_to_input) as file:
        for read_lines in range(4):
            line = file.readline()
            if read_lines == 1:
                try:
                    # Checking if the last character is a number
                    # to see if the file is written in color space
                    int(line.strip()[-1])

                    # Adding colorspace filenames to list
                    color_space_files.append(fq_file_name)

                    # Creating temporary file name
                    if re.match(r'.*_[1-2]', fq_file_name):
                        temp_fq_file_name = fq_file_name.replace('_', '-wasColorSpace_')
                    else:
                        temp_fq_file_name = fq_file_name + '-wasColorSpace'

                    output = directory + temp_fq_file_name + '.' + ext

                    print(color_print + "*** TRANSLATING SOLID COLORSPACE FILES ***" + white_print + "\n"
                          + "Translating " + fq_file_name + "." + ext + "\n")

                    # Translating  colorspace files with an imported perl script
                    os.system(f'perl {colorSpaceTranslation} -fastq {path_to_input} -o {output}')
                    os.remove(path_to_input)  # Removing untranslated files

                    # Renaming temporary filenames to old filenames
                    fq_file_name = temp_fq_file_name.replace("-wasColorSpace", "")
                    os.rename(output, path_to_input)

                # Checking if files contain corrupted characters
                except ValueError:
                    continue
            if read_lines == 3:
                if "/" in line:
                    corrupt_files.append(fq_file_name)


def prepare_for_trimming_files():
    """
    Preparing the fastq files for trimming:
        - Copying the gz files to output directory
        - Unpacking gz files
        - Checking if files are colorspace and if so translate them
    """
    # The extension is saved for further notice (fq/fastq), since this can differ
    ext = ""
    # All files in the fastqDir are obtained

    for fq_file in glob.glob(fastqDir + "*.gz"):
        # The filename, extension and the 'gz' are obtained
        file_name_comp = fq_file.split("/")[-1]
        fq_file_name, ext, gz = file_name_comp.split(".")
        out_dir = outputDir + "/RawData/fastqFiles/"
        new_path_to_gz = out_dir + file_name_comp
        new_path_fq_file = out_dir + fq_file_name + "." + ext

        # Copy-paste the gzipped file to the RawData directory
        if not os.path.exists(new_path_to_gz):
            subprocess.run("cp " + fq_file + " " + new_path_to_gz, shell=True)

        # The files are unzipped for the trimming, only when this is not done yet
        if not os.path.exists(new_path_fq_file):
            subprocess.run("gunzip -k " + new_path_to_gz, shell=True)

        # Making a list of unique file names
        unique_files.append(fq_file_name)

        # Checking if the files are colorspace and translate them
        translate_color_spaces(out_dir, fq_file_name, ext)

        # Making a lists of paired and single end files
        if re.match(r'.*_[1-2]', fq_file_name):
            pair = is_paired_file(fq_file_name)
            if fq_file_name.replace('_' + pair, '') not in paired_files:
                paired_files.append(fq_file_name.replace('_' + pair, ''))
        else:
            single_files.append(fq_file_name)
    return ext


def trim_files(ext):
    """
            The files are trimmed based on trim_galore.
            Trim_galore determined the cut-off based on the quality of the reads and removes the adapters if necessary.

        ...

        Attributes
        ------------
        ext : str
            directory where the genome hisat2 files are stored
    """
    for fq_file in single_files:
        fq_file_path = outputDir + "/RawData/fastqFiles/" + fq_file + "." + ext

        print(color_print + "*** PERFORM SINGLE END TRIMMING ***" + white_print + "\n")

        subprocess.run(trimGalore + " --path_to_cutadapt /usr/bin/cutadapt --length " + length + " " + fq_file_path
                       + " -o " + outputDir + "/Preprocessing/trimmed/", shell=True)

    for paired_fq_file in paired_files:
        print(color_print + "*** PERFORM PAIRED END TRIMMING ***" + white_print + "\n")
        fq_file_path = outputDir + "/RawData/fastqFiles/" + paired_fq_file

        subprocess.run(
            trimGalore + " --path_to_cutadapt /usr/bin/cutadapt --length " + length + " --paired " + fq_file_path +
            "_1." + ext + " " + fq_file_path + "_2." + ext + " --basename " + paired_fq_file + " -o " + outputDir +
            "/Preprocessing/trimmed/", shell=True)


def alignment(genome_hisat2):
    """ The actual alignment is performed in this function.
        The trimmed reads are obtained from the trimmed folder and are used of the alignment.
        The log file of the alignment is written to the Results/alignment folder and the .bam file is created.

        ...

        Attributes
        ------------
        genome_hisat2 : str
            directory where the genome hisat2 files are stored
    """

    for trimmed_fq_file in single_files:
        trimmed_fq_file_path = outputDir + "/Preprocessing/trimmed/" + trimmed_fq_file + "_trimmed.fq"

        print(color_print + "*** PERFORM SINGLE END ALIGNMENT ***" + white_print + "\n Aligning: " + trimmed_fq_file
              + ".fq \n")

        subprocess.run(f'{hisat} -p {threads} -x {genome_hisat2} -U {trimmed_fq_file_path} -S '
                       f'{outputDir+"/Preprocessing/samFiles/"+trimmed_fq_file+".sam"} 2>> '
                       f'{outputDir}/Results/alignment/{trimmed_fq_file}.log', shell=True)
        subprocess.call(f'samtools view -Sbo {outputDir}/Preprocessing/aligned/'
                        f'{trimmed_fq_file}.bam {outputDir + "/Preprocessing/samFiles/" + trimmed_fq_file + ".sam"}',
                        shell=True)

    for trimmed_paired_fq_file in paired_files:
        # The paired end data gets aligned with different settings
        trimmed_paired_fq_file_path = outputDir + "/Preprocessing/trimmed/" + trimmed_paired_fq_file

        print(color_print + "*** PERFORM PAIRED END ALIGNMENT ***" + white_print + "\n Aligning: "
              + trimmed_paired_fq_file + ".fq \n")

        subprocess.call(f'{hisat} -p {str(threads)} -x {genome_hisat2} -1 {trimmed_paired_fq_file_path + "_val_1.fq" }'
                        f' -2 {trimmed_paired_fq_file_path + "_val_2.fq"} -S '
                        f'{outputDir+"/Preprocessing/samFiles/"+trimmed_paired_fq_file+".sam"} '
                        f'2>> {outputDir}/Results/alignment/{trimmed_paired_fq_file}.log', shell=True)
        subprocess.call(f'samtools view -Sbo {outputDir}/Preprocessing/aligned/'
                        f'{trimmed_paired_fq_file}.bam '
                        f'{outputDir+"/Preprocessing/samFiles/"+ trimmed_paired_fq_file + ".sam"}', shell=True)


def preprocessing():
    """
        The step between the alignment and the creation of the count file is done in this function.
        These steps consist of the sorting, add or replace groups, fix mate information, merging,
        marking of duplicated and a sorting for the creation of the count files.
    """

    print(color_print + "*** PERFORM PROCESSING STEPS NECESSARY TO CREATE COUNT FILE ***" + white_print + "\n")

    for aligned_file in glob.glob(outputDir + '/Preprocessing/aligned/*.bam'):
        aligned_file_sep = aligned_file.split("/")
        current_file = aligned_file_sep[-1].replace(".bam", "")
        subprocess.run("java -jar " + picard + " SortSam I=" + outputDir + "/Preprocessing/aligned/" + current_file +
                       ".bam O=" + outputDir + "/Preprocessing/sortedBam/" + current_file + ".bam SO=queryname",
                       shell=True)
        subprocess.run(
            "java -jar " + picard + " AddOrReplaceReadGroups INPUT=" + outputDir + "/Preprocessing/sortedBam/"
            + current_file + ".bam OUTPUT=" + outputDir + "/Preprocessing/addOrReplace/" + current_file
            + ".bam " + " LB=" + current_file + " PU=" + current_file + " SM=" + current_file
            + " PL=illumina CREATE_INDEX=true", shell=True)
        subprocess.run(
            "java -jar " + picard + " FixMateInformation INPUT=" + outputDir + "/Preprocessing/addOrReplace/"
            + current_file + ".bam", shell=True)
        subprocess.run("java -jar " + picard + " MergeSamFiles INPUT=" + outputDir + "/Preprocessing/addOrReplace/"
                       + current_file + ".bam OUTPUT=" + outputDir + "/Preprocessing/mergeSam/" + current_file +
                       ".bam " + " CREATE_INDEX=true USE_THREADING=true", shell=True)
        subprocess.run("java -jar " + picard + " MarkDuplicates INPUT=" + outputDir + "/Preprocessing/mergeSam/"
                       + current_file + ".bam OUTPUT=" + outputDir + "/Preprocessing/markDuplicates/" + current_file
                       + ".bam " + " CREATE_INDEX=true METRICS_FILE=" + outputDir + "/Preprocessing/markDuplicates/"
                       + current_file + ".metrics.log", shell=True)
        subprocess.run("samtools sort -n " + outputDir + "/Preprocessing/markDuplicates/" + current_file + ".bam -o "
                       + outputDir + "/Preprocessing/markDuplicates/" + current_file + "_sorted.bam", shell=True)


def create_count_mat(gtf):
    """
        The last step is the creation of the count matrix file.
        This is done with the tool feature counts.
    """
    single_input_files = []
    for single_file in single_files:

        single_input_files.append(outputDir + "/Preprocessing/markDuplicates/"+single_file+"_sorted.bam")
    single_input_str = " ".join(single_input_files)

    subprocess.run(featureCounts + " -a " + gtf + " -o " + outputDir + "/RawData/counts/SingleEndGeneCounts.txt " +
                   single_input_str, shell=True)

    paired_input_files = []
    for paired_file in paired_files:
        paired_input_files.append(outputDir + "/Preprocessing/markDuplicates/" + paired_file + "_sorted.bam")
    paired_input_str = " ".join(paired_input_files)

    subprocess.run(featureCounts + " -a " + gtf + " -o " + outputDir + "/RawData/counts/PairedEndGeneCounts.txt " +
                   paired_input_str + " -p", shell=True)


def perform_multi_qc():
    """ Creating a multiqc in which all files are reported and plots are created."""
    print(color_print + "*** PERFORM MULTIQC STEP ***" + white_print + "\n")
    subprocess.run("multiqc " + outputDir + " -o " + outputDir + "/Results/multiQC/ " + "-p", shell=True)


def copy_paste_aligning_code():
    """ Copying and pasting the aligning code to the output directory."""
    print(color_print + "*** TRANSFER CODE FILES ***" + white_print + "\n")
    # Determine which files in the current working directory end with '.py'
    for files in glob.glob(os.getcwd() + "/*.py"):
        # Obtain the name of the files
        python_file = files.split("/")[-1]
        # Copy - paste the files to the Code/aligningPipeline directory
        subprocess.run("cp " + files + " " + outputDir + "/Code/aligningPipeline/" + python_file, shell=True)


def remove_folders():
    """ Removing folders that are no longer needed."""
    if os.path.exists(outputDir + "/Preprocessing/"):
        shutil.rmtree(outputDir + "/Preprocessing/")


def main():
    """
    This def acts as a main for the whole aligning process.
    The different functions are called here. """
    # Obtain all arguments when calling this script
    arguments()
    # Create the necessary directories for the study
    createDirs.createAllDirs(outputDir, organism)
    # The fastQC files are created
    quality_check()
    # Prepare for trimming, checking  for invalid files or translate them
    ext = prepare_for_trimming_files()
    # The files are trimmed based on the input of the user
    trim_files(ext)
    # Determine right genome annotation
    download_object = DownloadGenomeData(organism, outputDir)
    assembly_accession = download_object.assembly_name()
    # Downloading the fasta, gtf and HiSat2 files of the organisms genome
    genome_fasta = download_object.genome_file_name(assembly_accession, masking, level, chromosome_number)
    genome_gtf = download_object.gtf_file_name(assembly_accession)
    genome_hisat2 = download_object.genomeHiSat2(assembly_accession, genome_fasta)
    # Make sure that the fasta file of the right organism is chosen
    fasta_processing(genome_fasta)
    # Perform the alignment, given the HiSat2 genome
    alignment(genome_hisat2)
    # The preprocessing steps, the steps after the alignment and before the generation of the count files
    preprocessing()
    # Generation of the count matrix
    create_count_mat(genome_gtf)
    # Create the multiqc files
    perform_multi_qc()
    # Writing a report in a pdf generator
    pdf_generator = CreatePDF(outputDir, unique_files)
    pdf_generator.create_pdf(color_space_files, corrupt_files)
    # Copying the code to the output directory
    copy_paste_aligning_code()
    # Removing folders that are no longer needed
    remove_folders()

    print(color_print + "*** ALIGNING MAIN IS COMPLETE ***" + white_print + ''' 
If you used the pipeline properly, the files should be generated by now.
In the Result directory you can find files that depict the quality of
the samples in different timepoints (fastQC, alignment, etc.). This
directory also contains the pdf report with information about problems
that were encountered during the alingment.  

The pipeline has been modified by H. Dijkstra, K. Dijkstra, J. Kloosterman, 
    W. Rorije & W. Zeevat

The original pipeline was created by Marissa L. Dubbelaar 
    e-mail: (marissa.dubbelaar@hotmail.com)
    ''')


if __name__ == "__main__":
    main()
