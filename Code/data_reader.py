#!/usr/bin/env python3
"""
Authors: 	H. Dijkstra, K. Dijkstra, J. Kloosterman, W. Rorije & W. Zeevat
Date:		21-01-2021
Purpose:	Module containing functions for reading the results produced by the alignment pipeline
"""
import os
import glob


def get_alignment_result(directory):
    """
    Function that parses/processes alignment results. Returns two dictionaries containing results and reports
    when low rates of alignment percentage have been found.
    ...

    Attributes
    ------------
    directory : str
        directory where the alignment files are stored
    """
    # Initialize dictionaries
    alignment_results = {}
    problems = {}

    # Get filenames from alignment directory
    for log in os.listdir(directory + "/Results/alignment/"):
        with open(directory + "/Results/alignment/" + log) as logfile:

            # Initialize line counter
            line_count = 0

            for line in logfile:

                # Check if filename is in dictionary and add line
                if log.split(".")[0] not in alignment_results:
                    alignment_results[log.split(".")[0]] = [line.strip()]
                else:
                    alignment_results[log.split(".")[0]].append(line.rstrip())

                line_count += 1

                # Check line counter. If 6 report problem if rate of alignment is low and break loop
                if "overall alignment rate" in line:
                    if float(line.split("%")[0]) < 25:
                        percentage = float(line.split("%")[0])
                        problems[log.split(".")[0]] = f"Low alignment rate ({percentage}%). Make sure you are using " \
                                                      f"the correct reference genome. Or check if your files aren't " \
                                                      f"corrupted. There is also a possibility that the reads in " \
                                                      f"the files are too short and were deleted during trimming. " \
                                                      f"Check the summary table of {log} to see if there were a lot " \
                                                      f"of reads deleted."
                    break

    return alignment_results, problems


def get_count_results(outputdir):
    """
    Function that gets gene count results from geneCounts.txt.summary and returns file names and data in lists.
    ...

    Attributes
    ------------
    outputdir : str
        directory where the count files are stored
    """

    # initialize lists for storing data
    filenames = []
    data = {}

    # Get summary file from directory
    for summary_files in glob.glob(outputdir + "/RawData/counts/*.txt.summary"):

        with open(summary_files) as sum_file:
            # Get first line containing file paths
            header = sum_file.readline().split()

            # Add file names to list
            for file_path in header[1:]:
                filenames.append(file_path.split('/')[7][:-11])

            # Add matrix data to list
            for line in sum_file:
                if line.split()[0] not in data:
                    data[line.split()[0]] = line.split()[1:]
                else:
                    data[line.split()[0]] += line.split()[1:]

    return filenames, data


def get_trim_results(directory):
    """
    Parses/processes trim results and returns them in a dictionary
    """

    # Initialize dictionary
    trim_results = {}

    # Get filenames from directory
    for filename in os.listdir(directory + "trimmed/"):
        # Initialize flag for relevant sections
        flag = False
        if filename.endswith(".txt"):
            with open(directory + "trimmed/" + filename) as f:
                for line in f:

                    # Find relevant sections and place flag
                    if line.startswith("=== Summary ===") or line.startswith("RUN STATISTICS"):
                        flag = True
                    elif line.startswith("=== Adapter 1 ==="):
                        flag = False

                    # Add file name and add data. Filter out unnecessary lines
                    if flag:
                        if filename.split("_")[0] not in trim_results:
                            trim_results[filename.split("_")[0]] = []
                        else:
                            if line != "\n" and not line.startswith("===") and not line.startswith("RUN"):
                                trim_results[filename.split("_")[0]].append(line.strip())

    return trim_results


class SamFileCheck:
    """
    Class that performs some checks on the sam file
    """

    def mt_check(self, outputdir):
        """
        Checks the allignment (thus the sam file) to check how many
        reads align to the mitrochandrial dna
        ...

        Attributes
        ------------
        outputdir : str
            directory where the sam files are stored
        """
        results = {}
        for filename in glob.glob(outputdir + "/Preprocessing/samFiles/*.sam"):
            with open(filename) as samfile:

                non_mt = 0
                mt = 0

                for line in samfile:
                    if not line.startswith("@"):
                        location = line.split("\t")[2]
                        if location != "*":
                            if location == "MT":
                                mt += 1
                            else:
                                non_mt += 1
                try:
                    perc_mt = mt / (mt + non_mt) * 100

                except:
                    perc_mt = 0

            results[filename.split("/")[-1]] = [non_mt, mt, perc_mt]

        return results
