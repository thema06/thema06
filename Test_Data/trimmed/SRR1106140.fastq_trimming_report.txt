
SUMMARISING RUN PARAMETERS
==========================
Input filename: /homes/jkloosterman/Documents/leerjaar2/themaopdracht6/thema06/Test_Data/fastq_gz/SRR1106140.fastq
Trimming mode: single-end
Trim Galore version: 0.6.7
Cutadapt version: 3.2
Number of cores used for trimming: 1
Quality Phred score cutoff: 20
Quality encoding type selected: ASCII+33
Unable to auto-detect most prominent adapter from the first specified file (count Illumina: 0, count smallRNA: 0, count Nextera: 0)
Defaulting to Illumina universal adapter ( AGATCGGAAGAGC ). Specify -a SEQUENCE to avoid this behavior).
Adapter sequence: 'AGATCGGAAGAGC' (Illumina TruSeq, Sanger iPCR; default (inconclusive auto-detection))
Maximum trimming error rate: 0.1 (default)
Minimum required adapter overlap (stringency): 1 bp
Minimum required sequence length before a sequence gets removed: 20 bp


This is cutadapt 3.2 with Python 3.9.2
Command line parameters: -j 1 -e 0.1 -q 20 -O 1 -a AGATCGGAAGAGC /homes/jkloosterman/Documents/leerjaar2/themaopdracht6/thema06/Test_Data/fastq_gz/SRR1106140.fastq
Processing reads on 1 core in single-end mode ...
Finished in 0.00 s (79 µs/read; 0.76 M reads/minute).

=== Summary ===

Total reads processed:                      50
Reads with adapters:                         0 (0.0%)
Reads written (passing filters):            50 (100.0%)

Total basepairs processed:         1,458 bp
Quality-trimmed:                   1,458 bp (100.0%)
Total written (filtered):              0 bp (0.0%)

=== Adapter 1 ===

Sequence: AGATCGGAAGAGC; Type: regular 3'; Length: 13; Trimmed: 0 times

RUN STATISTICS FOR INPUT FILE: /homes/jkloosterman/Documents/leerjaar2/themaopdracht6/thema06/Test_Data/fastq_gz/SRR1106140.fastq
=============================================
50 sequences processed in total
Sequences removed because they became shorter than the length cutoff of 20 bp:	50 (100.0%)

